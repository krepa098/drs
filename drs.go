// Copyright 2013 by krepa098. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//This package is meant to extract data from a drs file used by Age of Empires 2.
//Thanks to Stoyan Ratchev for his description of the drs format.
package drs

import (
	"encoding/binary"
	"io"
	"os"
	"errors"
)

//Type IDs.
const (
	TypeBin = 0x62696E61 //'b','i','n','a' -> bin
	TypeSlp = 0x736C7020 //'s','l','p',' ' -> slp
	TypeShp = 0x73687020 //'s','h','p',' ' -> shp
	TypeWav = 0x77617620 //'w','a','v',' ' -> wav
)

type FileData struct {
	Header  Header
	Tables  []TableInfo
	Entries [][]TableEntry //Entries[table][tableEntry]
	data    []byte
}

type Header struct {
	CopyrightInfo [40]byte //Copyright (c) 1997 Ensemble Studios
	Version       [4]byte  //1.00
	FileType      [12]byte //tribe
	TableCount    int32
	DataOffset    int32
}

type TableInfo struct {
	Type      int32 //bin, shp, slp or wav
	Offset    int32
	FileCount int32 //Number of files in the table
}

type TableEntry struct {
	Id     int32 //Unique id of the entry i.e. 644 = Janissary moving
	Offset int32
	Size   int32 //Size of the entry
}

//Decodes the whole drs file and populates a FileData struct.
func Decode(reader io.ReadSeeker) (drsFile FileData) {
	size, _ := reader.Seek(0, os.SEEK_END)
	reader.Seek(0, os.SEEK_SET)

	drsFile.Header = decodeHeader(reader)

	//read table info
	drsFile.Tables = make([]TableInfo, int(drsFile.Header.TableCount))
	for i := 0; i < int(drsFile.Header.TableCount); i++ {
		binary.Read(reader, binary.LittleEndian, &drsFile.Tables[i])
	}

	//read table entries
	for i := 0; i < int(drsFile.Header.TableCount); i++ {
		drsFile.Entries = append(drsFile.Entries, make([]TableEntry, drsFile.Tables[i].FileCount))

		for j := 0; j < int(drsFile.Tables[i].FileCount); j++ {
			binary.Read(reader, binary.LittleEndian, &drsFile.Entries[i][j])
		}
	}

	//check position
	pos, _ := reader.Seek(0, 1)
	if pos != int64(drsFile.Header.DataOffset) {
		panic("invalid data position")
	}
	//read data
	drsFile.data = make([]byte, size-pos)
	reader.Read(drsFile.data)

	return
}

//Decodes the drs file header.
func decodeHeader(reader io.ReadSeeker) (header Header) {
	binary.Read(reader, binary.LittleEndian, &header)
	return
}

//Opens a drs file and calls Decode on it.
func DecodeFromFile(filename string) FileData {
	file, _ := os.Open(filename)
	defer file.Close()

	return Decode(file)
}

//Returns the chunk of data of the entry.
func (this *FileData) GetFileData(table, entry int) []byte {
	dataOffset := this.Header.DataOffset
	entryOffset := this.Entries[table][entry].Offset
	entrySize := this.Entries[table][entry].Size
	return this.data[entryOffset-dataOffset : entryOffset-dataOffset+entrySize]
}

//Returns the index and the table of an ID
func (this *FileData) GetIndexFromId(id int) (table, index int, err error) {
	for i := 0; i < int(this.Header.TableCount); i++ {
		for j := 0; j < int(this.Tables[i].FileCount); j++ {
			if this.Entries[i][j].Id == int32(id) {
				return i, j, nil
			}
		}
	}
	return -1, -1, errors.New("ID not found")
}

//Maps the type id to string (bin,slp,shp,wav).
func TypeToString(t int32) string {
	switch t {
	case TypeBin:
		return "bin"
	case TypeSlp:
		return "slp"
	case TypeShp:
		return "shp"
	case TypeWav:
		return "wav"
	}
	return "unknown"
}
